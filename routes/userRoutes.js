var express = require('express');
const multer = require('multer');
const passport =require('passport')
var router = express.Router();
var userController = require('../controllers/userController');
var passportMiddleware = require('../middlewares/passportMiddleware');
//Multer Setup Started
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src')
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' +file.originalname)
  }
})
const upload = multer({ storage: storage }).array('file');
//Multer Setup Ended


function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) return next();
  res.send({
    message:"you need to be logged in"
  });
}
//User APIs Started
//Route for sending registration OTP
router.post('/sendotp', userController.sendOtp)
//Route for verifying registration OTP
router.post('/confirmotp', userController.confirmOtp)
//Route for registering after verification
router.post('/',upload, userController.createUser)
//Route for getting all users
router.get('/', userController.getUsers)
//Route for login user
router.post('/login',passportMiddleware.passportAuthenticate);
//Route for logout user
router.get('/logout', userController.logout)
//Route for getting forget password otp on phonenumber
router.post('/forgetpassword', userController.forgetPassword)
//Route for getting forget password otp on email
router.post('/forgetpassword2', userController.forgetPassword2)
//Route for verifying forget password otp
router.post('/verifyotp', userController.verifyOTP)
//Route for setting new password
router.post('/resetpassword', userController.resetPassword)
//Route for getting user by ID
router.get('/getuser/:id', userController.getUser)
//Route for deleting user by ID
router.delete('/deleteuser/:id', userController.deleteUser)
//Route for updating user by ID
router.patch('/updateuser/:id',upload, userController.updateUser)
//Route for getting user profile picture by ID
router.get('/getuserpicture/:id', userController.getUserpicture)
//User APIs Ended
module.exports = router;