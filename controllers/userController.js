var userModel = require('../models/userModel')
var passwordModel = require('../models/passwordModel')
var otpModel = require('../models/otpModel')
const bcrypt = require("bcrypt")
var ObjectId = require('mongodb').ObjectID;
const nodemailer = require("nodemailer");
const crypto  = require('crypto')
const saltRounds = 10;
const otpGenerator = require('otp-generator')
var accountSid = process.env.TWILIO_ACCOUNT_SID;
var authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);
const { authSchema,changepasswordSchema,forgetpasswordSchema,forgetpasswordSchema2,otpSchema,resetotpSchema,sendotpSchema,updateSchema } = require('../helpers/userValidationSchema')
const sendOtp=async(req,res)=>{ 
  try {
    const result = await sendotpSchema.validateAsync(req.body)
      //Creating OTP for SMS
      var otp=otpGenerator.generate(6, { digits:true, upperCaseAlphabets: false, specialChars: false });
      console.log("otp : ",otp)
      //Twilio SMS Started
      console.log("countrycode: ",req.body.countrycode+"Phone",req.body.countrycode)
      const number=req.body.countrycode+''+req.body.phoneno
      console.log("numberrr: ",number)
      //Sending OTP to upcoming user number for register verification
      client.messages.create({
        body: `Add This OTP ${otp} to register`,
        to: `+${number}`,
        from: '+19206206746'
      }).then(async message => {
        console.log(message)
        otpModel.findOne({ phoneno: req.body.phoneno})
        .then(async userr =>{
          if(userr){
            console.log('user', userr)
            const updatedOtp=await otpModel.updateOne(
              {phoneno: req.body.phoneno},
              {
                $set:{
                  otp:otp,
                }
              }
            );
            if(updatedOtp){
              res.status(200).json({
                success:true,
                message:"Registration Otp send"
              })
            }
            
          }else{
            //Saving OTP in otps collection for later verification
            const newOtp = await new otpModel({
              countrycode: req.body.countrycode,
              phoneno: req.body.phoneno,
              otp: otp,
            }).save();
            if(newOtp){
              res.status(200).json({
                success:true,
                message:"Registration Otp send"
              })
            }

          }
          
        })
        
        
        
      })
      .catch(error => {
        console.log(error)
        res.status(404).json({
          success:false,
          message:"Number Not Valid"
        })
      })
      //Twilio SMS Ended
  }
  catch(err) {
    console.log(err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
    console.log(err)
  }
  
   
}
const confirmOtp=async(req,res)=>{ 
  try {
    const result = await otpSchema.validateAsync(req.body)
    console.log(req.body.otp)
    //Finding number with given otp
    otpModel.findOne({ otp: req.body.otp })
    .then(otpProfile => {
      console.log('user', otpProfile)
      //Find if any otp exists
      if(otpProfile){
        res.status(200).send({
          success:true,
          otpProfile:otpProfile,
          message:"OTP Successful, User Is Logged-In"
        })
      }else{
        //send fail response if otp doesn't exists
        res.status(404).send({
          success:false,
          message:"OTP Failed"
        })
      }
    })
  }
  catch(err) {
    console.log(err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  }
  
}
const createUser = async(req,res)=>{ 
  try {
    console.log("req.body is", req.body);
    const result = await authSchema.validateAsync(req.body)
    //checking if user exists
    const ifuser=await userModel.findOne({$or:[{email: req.body.email},{phoneno:req.body.phoneno}] })
    if(ifuser){
      res.status(409).send({
      success: false,
      message:"User Already Exists with this email/phone"
    });
  }else{
    //encrypting user password
    const encryptedPassword =await bcrypt.hash(req.body.password, saltRounds)
    //saving user to DB
    console.log("req.files: ",req.files.length)
    const newUser = await new userModel({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      address:req.body.address,
      countrycode:req.body.countrycode,
      phoneno:req.body.phoneno,
      profilepic:req.files?"/src/"+req.files.length>0?req.files[0].filename:null:null
    }).save();
    if(newUser){
      console.log("You are now user",newUser)
      const newPassword = await new passwordModel({
        user: newUser._id,
        password: encryptedPassword
      }).save();
      res.status(200).send({
        success: true,
        message:"You are now user"
      });
    }else{
      console.log("Request Failed")
      res.status(404).send({
        success: false,
        message:"Request Failed"
      });
    }
  }
  }
  catch(err) {
    console.log("err.isJoi: ",err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  }
  

    
}
const getUsers=async(req,res)=>{ 
  try {
    console.log("getting all users")
    const availableElements = await userModel.find({})
    .catch((err) => {
      res.status(400).json({message: err.message});
    });
    res.status(200).send(availableElements);
  }
  catch(err) {
    console.log(err)
  }
  
   
}
/** 
const login=async(req,res)=>{ 
  console.log("Body ", req.body);
  const user=await userModel.findOne({ email: req.body.email })
  if(user){
    if(await bcrypt.compare(req.body.password, user.password)){
      
      return res.status(200).send({
        success: true,
        message: 'Correct Details',
        user: user
      });


    }else{
      return res.status(404).send({
        success: false,
        message: 'Error: Email and Pass Dont Match'
      });
    }

    
  }else{
    console.log("Invalid User");
        return res.status(404).send({
          success: false,
          message: 'User not exists'
        });

  }
}
*/

const logout=async(req,res)=>{ 
  try {
    console.log("logging out")
    req.logout();
    userModel.findOne({ phoneno: req.body.phoneno})
    .then(userr => {
      console.log('user', userr)
      return userr.save()
    })
    return res.status(200).json({
      success:true,
      message:'User Logged Out'
    })
  }
  catch(err) {
    console.log(err)
  }
  
}
const forgetPassword=async(req,res)=>{ 
  try {
    console.log("U are ", req.body);
    const result = await forgetpasswordSchema.validateAsync(req.body)
    console.log("result",result)
    userModel.findOne({ phoneno: req.body.phoneno })
    .then(user => {
      console.log('user', user)
      //Checking If User Exists
      if (!user) {
        return res.status(404).json({
          success:false,
          message:'User not found with this number!'
        })
      } 
      //Creating Reset OTP for SMS
      var otp=otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
      const number=req.body.countrycode+''+req.body.phoneno
      console.log("numberrr: ",number)
      //Sending Reset OTP to user number
      client.messages.create({
        body: `Reset Password OTP: ${otp} `,
        to: `+${number}`,
        from: '+19206206746'
      })
      .then(message => console.log(message))
      .catch(error => console.log(error))
      user.resetPasswordOtp = otp;
      return user.save()
    })
    .then(result=>{
      res.status(200).send({
        success:true,
        message:"Reset Password OTP sent"
      })
    })
    .catch(err => {
      console.log(err)
    })
  }
  catch(err) {
    console.log("err.isJoi: ",err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  }
  
}
const forgetPassword2=async(req,res)=>{ 
  try {
    console.log("U are ", req.body);
    const result = await forgetpasswordSchema2.validateAsync(req.body)
    console.log("result",result)
    userModel.findOne({ email: req.body.email })
    .then(user => {
      console.log('user', user)
      //Checking If User Exists
      if (!user) {
        return res.status(404).json({
          success:false,
          message:'User not found with this Email!'
        })
      } 
      //Creating Reset OTP for SMS
      var otp=otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
      const number=req.body.countrycode+''+req.body.phoneno
      console.log("numberrr: ",number)
      //Sending Reset OTP to email
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASSWORD
        }
    })
    let mailOptions = {
        from: 'JobWeb@gmail.com',
        to: req.body.email,
        subject: 'Reset Password',
        html: `Reset Password OTP: ${otp} `
    }
    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            return console.log('error occurs', err)
        }
        
    })
      user.resetPasswordOtp = otp;
      return user.save()
    })
    .then(result=>{
      
      res.status(200).send({
        success:true,
        message:"Reset Password Email sent"
      })
    })
    .catch(err => {
      console.log(err)
    })
  }
  catch(err) {
    console.log("err.isJoi: ",err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  }
  
}
const verifyOTP=async(req,res)=>{
  try {
    console.log("U are ", req.body);
    //Finding user with the reset OTP
    const result = await resetotpSchema.validateAsync(req.body)
    userModel.findOne({ resetPasswordOtp: req.body.resetPasswordOtp })
    .then(user => {
      //If User don't exist with the given resetOTP, give error
      if (!user) {
        return res.status(404).json({
          success:false,
          message:'Wrong Reset Password OTP'
        })
      }else{
        //If User exists with the given resetOTP then send success
        return res.status(200).json({
          success:true,
          user:user,
          message:'OTP Verified. User Can Change The Password'
        })
      }
    })
  }
  catch(err) {
    console.log(err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  } 
  
}
const resetPassword=async(req,res)=>{
  try {
    console.log('req.body', req.body)
    const result = await changepasswordSchema.validateAsync(req.body)
    const password = req.body.password;
    const confirmPassword = req.body.confirmpassword;
    //Checking If new password and confirm password equals
    if(password!==confirmPassword){
      return res.status(404).json('Password and Confirm Password Not Equal')
    }
    const user=userModel.findOne({"_id": ObjectId(req.body.userid)})
    if (!user) {
      return res.status(404).json('User not found!')
    }
    try{
      //Encrypting new password
      let encryptedPassword = await bcrypt.hash(req.body.password, saltRounds);
      console.log("encryptedPassword: ",encryptedPassword)
      //Updating password
      const updatePassword=await userModel.updateOne(
        {_id:req.body.userid},
        {
          $set:{
            resetPasswordOtp:null
          }
        }
      );
      const updatePassword1=await passwordModel.updateOne(
        {user:req.body.userid},
        {
          $set:{
            password:encryptedPassword
          }
        }
      );
    console.log("updatePassword: ",updatePassword)
    return res.status(200).json({
      success:true,
      message:'Password Updated'
    })
  }
  catch(err){
    res.status(500).json({
      success:false,
      message:'internal server error'
    })
  }
  }
  catch(err) {
    console.log("err.isJoi: ",err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  } 
  
}
const getUser=async(req,res)=>{
  try {
    console.log("get userID: ",req.params.id)
    //getting user by ID
    const user=await userModel.findOne({"_id": ObjectId(req.params.id)});
    //sending user to client if recieved
    if(user){
      return res.status(200).send({
        success: true,
        message: 'Correct Details',
        user: user
      });
    }else{
      //sending Error to client if not recieved
      return res.status(404).send({
        success: false,
        message: 'User Not Found'
      });
    }
  }
  catch(err) {
    console.log("err ",err)
  } 
  
}
const deleteUser=async(req,res)=>{
  try {
    console.log("get userID: ",req.params.id)
    //soft deleting user by ID 
    const user=await userModel.softDelete({"_id": ObjectId(req.params.id)});
    const password=await passwordModel.softDelete({"user": ObjectId(req.params.id)});
    if(user){
      //Sending success if user deleted successfully
      return res.status(200).send({
        success: true,
        message: 'User Deleted',
        user: user
      });
    }else{
      //Sending failure if user don't exists by given ID and cant be deleted
      return res.status(404).send({
        success: false,
        message: 'No User By Given ID'
      });
    }
  }
  catch(err) {
    console.log("err",err)
  } 
  
}
const updateUser=async(req,res)=>{
  try {
    console.log("get userID: ",req.params.id)
    console.log("req.body is", req.body);
    //Validating recieved user fields
    const result = await updateSchema.validateAsync(req.body)
    //Finding user who is being updated
    const user=await userModel.findOne({"_id": ObjectId(req.params.id)});
    console.log(user)
    if(!user){
      res.status(404).send({
        success:false,
        message:"User Don't Exists!"
      })
    }
    //Checking if we got any image
    if(req.files){
      console.log("we getting image")
      console.log(req.files.length)
    }else{
      console.log("we not getting image")
    }
    console.log("req.body.firstname",req.body.firstname)
    //Updating user fields with new values
    const updatedUser=await userModel.updateOne(
      {_id:req.params.id},
      {
        $set:{
          firstname:req.body.firstname?req.body.firstname:user.firstname,
          lastname:req.body.lastname?req.body.lastname:user.lastname,
          email:req.body.email?req.body.email:user.email,
          address:req.body.address?req.body.address:user.address,
          countrycode:req.body.countrycode?req.body.countrycode:user.countrycode,
          phoneno:req.body.phoneno?req.body.phoneno:user.phoneno,
          profilepic:req.files?"/src/"+req.files[0].filename:user.profilepic
        }
      }
    );
    //Checking if we recieved password
    if(req.body.password){
      //Encrypting user password
      const encryptedPassword =await bcrypt.hash(req.body.password, saltRounds)
      console.log(encryptedPassword)
      console.log("adding new password")
      //Updating new user password
      const updatedPassword=await passwordModel.updateOne(
        {user:req.params.id},
        {
          $set:{
            password:encryptedPassword
          }
        }
      );
    }
    
    if(updatedUser){
      //Sending success if user updated 
      return res.status(200).json({
        success:true,
        message:"User Updated!",
        updatedUser
      })
    }else{
      //Sending failure if user not updated 
      return res.status(400).json({
        success:false,
        message:"the user cannot be updated!"
      })
    }
  }
  catch(err) {
    console.log("err.isJoi: ",err)
    if(err.isJoi){
      res.status(422).json({
        success:false,
        message:err.details[0].message
      })
    }
  } 
  
}
const getUserpicture=async(req,res)=>{
    try {
      console.log("get userID: ",req.params.id)
      //Getting profile picture of user
      const user=await userModel.findOne({"_id": ObjectId(req.params.id)}).select('profilepic');
      user.profilepic=user.profilepic?process.env.SERVER_URL+user.profilepic:null
      if(user){
        //Sending user's profile picture if exists
        return res.status(200).send({
          success: true,
          message: 'Correct Details',
          user: user
        });
      }else{
        //Sending failure if not exists
        return res.status(404).send({
          success: false,
          message: 'User Not Found'
        });
      }
    }
    catch(err) {
      console.log("err.isJoi: ",err)
    } 
  
}
module.exports = {
  createUser,
  getUsers,
  logout,
  confirmOtp,
  forgetPassword,
  forgetPassword2,
  verifyOTP,
  resetPassword,
  getUser,
  deleteUser,
  updateUser,
  getUserpicture,
  sendOtp
};