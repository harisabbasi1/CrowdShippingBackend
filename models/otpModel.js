var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 var otp = new Schema({
    "countrycode":{
        type: Number,
        required: true
       },
    "phoneno":{
        type: Number,
        required: true},
    "otp":String,
});
module.exports = mongoose.model('otps', otp);