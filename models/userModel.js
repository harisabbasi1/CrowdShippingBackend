var mongoose = require('mongoose');
const { softDeletePlugin } = require('soft-delete-plugin-mongoose');
var Schema = mongoose.Schema;
 var user = new Schema({
     "firstname": String, 
     "lastname":String,
     "email":{
         type: String,
         required: true
        },
     "countrycode":{
         type: Number,
         required: true
        },
     "phoneno":{
         type: Number,
         required: true},
     "address":String,
     "resetPasswordOtp":String,
     "profilepic":String
    
});
user.plugin(softDeletePlugin)
module.exports = mongoose.model('users', user);